import requests as r
import pandas as pd
from io import StringIO

ONLINE_URL = 'https://mediametrics.ru/rating/ru/online.tsv?page=1&per_page=100'
HOUR_URL = 'https://mediametrics.ru/rating/ru/hour.tsv?page=1&per_page=100'
DAY_URL = 'https://mediametrics.ru/rating/ru/day.tsv?page=1&per_page=100'
WEEK_URL = 'https://mediametrics.ru/rating/ru/week.tsv?page=1&per_page=100'

def get_tsv(url):
    resp = r.get(url)
    return resp.text

def get_json(url):
    tsv = get_tsv(url)
    df = pd.read_csv(StringIO(tsv), sep='\t')
    return df.to_json(force_ascii=False, orient='records')

def get_online_json():
    return get_json(ONLINE_URL)

def get_hour_json():
    return get_json(HOUR_URL)

def get_day_json():
    return get_json(DAY_URL)

def get_week_json():
    return get_json(WEEK_URL)