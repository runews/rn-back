import urllib.parse
import sys
import requests as r

#TEMPLATE = "http://boilerpipe-web.appspot.com/extract?url={0}&extractor=ArticleExtractor&output=json&extractImages=&token="
TEMPLATE = "http://spark.dimkk.ru:3030/article?url={0}"


def get_json(url):
    ext_url = TEMPLATE.format(url)
    print(ext_url)
    resp = r.get(ext_url)
    return resp.text

def get_text_json(url):
    return get_json(url)