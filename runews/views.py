from django.http import HttpResponse
from . import mm
from . import text_extractor

def online(request):
    return HttpResponse(mm.get_online_json(), content_type="application/json")

def hour(request):
    return HttpResponse(mm.get_hour_json(), content_type="application/json")

def day(request):
    return HttpResponse(mm.get_day_json(), content_type="application/json")

def week(request):
    return HttpResponse(mm.get_week_json(), content_type="application/json")

def text(request):
    return HttpResponse(text_extractor.get_text_json(request.GET.get('url', '')), content_type="application/json")